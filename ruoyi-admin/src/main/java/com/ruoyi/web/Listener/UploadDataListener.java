package com.ruoyi.web.Listener;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.read.listener.ReadListener;
import com.ruoyi.web.domin.DemoData;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 模板的读取类
 *
 * @author Jiaju Zhuang
 */
// 有个很重要的点 DemoDataListener 不能被spring管理，要每次读取excel都要new,然后里面用到spring可以构造方法传进去
@Slf4j
public class UploadDataListener implements ReadListener<DemoData> {
    /**
     * 每隔5条存储数据库，实际使用中可以100条，然后清理list ，方便内存回收
     */

    private List<DemoData> cachedDataList = new ArrayList<>();
    /**
     * 假设这个是一个DAO，当然有业务逻辑这个也可以是一个service。当然如果不用存储这个对象没用。
     */
    private DemoData demoData;

    public UploadDataListener() {
        // 这里是demo，所以随便new一个。实际使用如果到了spring,请使用下面的有参构造函数
        demoData = new DemoData();
    }

    /**
     * 如果使用了spring,请使用这个构造方法。每次创建Listener的时候需要把spring管理的类传进来
     *
     * @param
     */
    public UploadDataListener(DemoData demoData) {
        this.demoData = demoData;
    }


    /**
     * 这个每一条数据解析都会来调用
     *
     * @param data    one row value. It is same as {@link AnalysisContext#readRowHolder()}
     * @param context
     */
    @Override
    public void invoke(DemoData data, AnalysisContext context) {
        //log.info("解析到一条数据:{}", JSON.toJSONString(data));
        //log.info("读取数据");
        cachedDataList.add(data);
        // 达到BATCH_COUNT了，需要去存储一次数据库，防止数据几万条数据在内存，容易OOM

//log.info("--------------------------抽查完成------------------------------");

    }

    /**
     * 所有数据解析完成了 都会来调用
     *
     * @param context
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext context ) {
        //参数集合
        //ArrayList<DemoData> data = new ArrayList<DemoData>();

        List<DemoData> newData = new ArrayList<DemoData>();

        // 这里也要保存数据，确保最后遗留的数据也存储到数据库
        //saveData();
        String fileName = "C://Users//86151//Desktop//测试.xlsx";

        Map<String, List<DemoData>> map = cachedDataList.stream().filter(o -> o.getCity() != null).collect(Collectors.groupingBy(DemoData::getCity));

extracted(newData,map);
        //System.out.println(cachedDataList);
        EasyExcel.write(fileName, DemoData.class).sheet("抽查结果").doWrite(newData);

        log.info("所有数据解析完成！");
    }



    private static void extracted(List<DemoData> newData, Map<String, List<DemoData>> map) {
        map.forEach((city, v) -> {
            Map<String, List<DemoData>> daiWeiMap = v.stream().collect(Collectors.groupingBy(DemoData::getDaiWei));
            //返回键值对数量
            int size = daiWeiMap.size();
            if (size == 0) {
                return;
            }

            if (size == 1) {
                daiWeiMap.forEach((s, values) -> {
                    Collections.shuffle(values);
                    Collections.shuffle(values);
                    if (values.size() < 65) {
                        newData.addAll(values);

                    } else {
                        for (int i = 0; i < 64; i++) {
                            newData.add(values.get(i));
                        }
                    }
                });
            }
            if (size == 2) {
                daiWeiMap.forEach((s, values) -> {
                    Collections.shuffle(values);
                    Collections.shuffle(values);
                    if (values.size() < 33) {
                        newData.addAll(values);

                    } else {
                        for (int i = 0; i < 32; i++) {
                            newData.add(values.get(i));
                        }
                    }
                });
            }
            if (size == 3) {
                //Set<String> keySet = daiWeiMap.keySet();
                //List<String> list = keySet.stream().collect(Collectors.toList());
                //List<DemoData> demoData = daiWeiMap.get(list.get(list.size() - 1));
                //newData.add(demoData.get(demoData.size()-1));
                //15
                List<List<DemoData>> collect = daiWeiMap.values().stream().collect(Collectors.toList());

                Collections.shuffle(collect);

                List<DemoData> demoData1 = collect.get(collect.size()/2);
//在打乱一遍
                Collections.shuffle(demoData1);

                newData.add(demoData1.get(demoData1.size()/2));

                //daiWeiMap.forEach((s, values) -> {
                //    newData.add(values.get(values.size()-1));
                //});

                daiWeiMap.forEach((s, values) -> {

                    Collections.shuffle(values);
                    Collections.shuffle(values);
                    if (values.size() < 22) {
                        newData.addAll(values);
                    } else {
                        for (int i = 0; i < 21; i++) {
                            newData.add(values.get(i));
                        }
                    }


                });
            }

            if (size == 4) {
                daiWeiMap.forEach((s, values) -> {
                    Collections.shuffle(values);
                    Collections.shuffle(values);
                    if (values.size() < 17) {
                        newData.addAll(values);
                    } else {
                        for (int i = 0; i < 16; i++) {
                            newData.add(values.get(i));
                        }
                    }
                });
            }

            if (size == 5) {

                //Set<String> keySet = daiWeiMap.keySet();
                //List<String> list = keySet.stream().collect(Collectors.toList());
                //List<DemoData> demoData = daiWeiMap.get(list.get(list.size() - 1));
                //newData.add(demoData.get(demoData.size()-1));

                List<List<DemoData>> collect = daiWeiMap.values().stream().collect(Collectors.toList());

                Collections.shuffle(collect);

                List<DemoData> demoData1 = collect.get(collect.size()/2);
                //在打乱一遍
                Collections.shuffle(demoData1);

                newData.add(demoData1.get(demoData1.size()/2));

                daiWeiMap.forEach((s, values) -> {

                    Collections.shuffle(values);
                    Collections.shuffle(values);
                    if (values.size() < 13) {
                        newData.addAll(values);
                    } else {
                        for (int i = 0; i < 12; i++) {
                            newData.add(values.get(i));

                        }

                    }

                });

            }

        });
    }


}
