package com.ruoyi.web.service.impl;

import com.alibaba.excel.EasyExcel;
import com.ruoyi.web.Listener.UploadDataListener;
import com.ruoyi.web.domin.DemoData;
import com.ruoyi.web.service.CommonService;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.lang.System.out;

@Service
public class CommonServiceImpl implements CommonService {


    @Override
    public void download(HttpServletResponse response) {

        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
//        测试一下，本地的文件是否能下载
        String fileName = "测试";
//        String fileName = "";
        try {
            fileName = URLEncoder.encode("测试", "UTF-8").replaceAll("\\+", "%20");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
        //try {
        //    EasyExcel.write(response.getOutputStream(), DemoData.class).sheet("模板").doWrite(data());
        //} catch (IOException e) {
        //    e.printStackTrace();
        //}


    }
    //文件上传

    @Override
    public void upload(MultipartFile file) {

        DemoData demoData = new DemoData();

        try {
            EasyExcel.read(file.getInputStream(), DemoData.class, new UploadDataListener(demoData)).sheet().doRead();



        } catch (IOException e) {
            e.printStackTrace();
        }

    }



    private List<DemoData> data() {

        List<DemoData> newData = null;
        try {
// "C://Users//86151//Desktop//测试.xlsx";
            String fileName = "C://Users//Administrator//Desktop//任务维度详情-导出(2).xlsx";
            //创建工作簿对象
            //C:\Users\Administrator\Desktop\测试.xlsx  C:\Users\Administrator\Desktop\
            XSSFWorkbook workbook = new XSSFWorkbook(new FileInputStream(fileName));


            //参数集合
            ArrayList<DemoData> data = new ArrayList<DemoData>();

            newData = new ArrayList<DemoData>();

            //sheet页
            Sheet sheet = workbook.getSheetAt(1);

            for (Row row : sheet) {
                DemoData demoData = new DemoData();

                Cell city = row.getCell(1);
                Cell daiWei = row.getCell(3);

                Cell string1 = row.getCell(4);
                Cell string2 = row.getCell(5);
                Cell string3 = row.getCell(6);

                Cell plan = row.getCell(7);

                Cell string4 = row.getCell(7);
                Cell string5 = row.getCell(9);
                Cell date1 = row.getCell(10);
                Cell date2 = row.getCell(11);

                Cell string8 = row.getCell(12);
                Cell string9 = row.getCell(13);
                Cell string10 = row.getCell(14);


                Cell string11 = row.getCell(15);
                Cell string12 = row.getCell(16);


                if (!StringUtils.isEmpty(city)) {
                    demoData.setCity(city.toString());
                }
                if (!StringUtils.isEmpty(daiWei)) {
                    demoData.setDaiWei(daiWei.toString());
                }
                if (!StringUtils.isEmpty(string1)) {
                    demoData.setString1(string1.toString());
                }
                if (!StringUtils.isEmpty(string2)) {
                    demoData.setString2(string2.toString());
                }
                if (!StringUtils.isEmpty(string3)) {
                    demoData.setString3(string3.toString());
                }
                //计划
                if (!StringUtils.isEmpty(plan)) {
                    demoData.setPlan(plan.toString());
                }

                if (!StringUtils.isEmpty(string4)) {
                    demoData.setString4(string4.toString());
                }
                if (!StringUtils.isEmpty(string5)) {
                    demoData.setString5(string5.toString());
                }


                //日期
                if (!StringUtils.isEmpty(date1)) {
                    demoData.setDate1(date1.toString());
                }
                if (!StringUtils.isEmpty(date2)) {
                    demoData.setDate2(date2.toString());
                }


                if (!StringUtils.isEmpty(string8)) {
                    demoData.setString8(string8.toString());
                }

                if (!StringUtils.isEmpty(string9)) {
                    demoData.setString9(string9.toString());
                }
                if (!StringUtils.isEmpty(string10)) {
                    demoData.setString10(string10.toString());
                }
                if (!StringUtils.isEmpty(string11)) {
                    demoData.setString11(string11.toString());
                }
                if (!StringUtils.isEmpty(string12)) {
                    demoData.setString12(string12.toString());
                }
                data.add(demoData);
                out.println("写入数据成功");

            }

            //按规则处理数据
            //Map<String, DemoData> collect = data.stream().collect(Collectors.toMap(DemoData::getCity, o -> o));
            Map<String, List<DemoData>> map = data.stream().filter(o -> o.getCity() != null).collect(Collectors.groupingBy(DemoData::getCity));
            //分组，每个组取16条  但是要求每个维度下，取5个
            extracted(newData, map);

            //EasyExcel.write(fileName, DemoData.class).sheet("抽查结果").doWrite(newData);

            out.println("---------------------------------完成----------------------------");

        } catch (Exception e) {
            e.printStackTrace();
        }

        return newData;
    }

    private static void extracted(List<DemoData> newData, Map<String, List<DemoData>> map) {
        map.forEach((city, v) -> {
            Map<String, List<DemoData>> daiWeiMap = v.stream().collect(Collectors.groupingBy(DemoData::getDaiWei));
            //返回键值对数量
            int size = daiWeiMap.size();
            if (size == 0) {
                return;
            }

            if (size == 1) {
                daiWeiMap.forEach((s, values) -> {
                    Collections.shuffle(values);
                    Collections.shuffle(values);
                    if (values.size() < 65) {
                        newData.addAll(values);

                    } else {
                        for (int i = 0; i < 64; i++) {
                            newData.add(values.get(i));
                        }
                    }
                });
            }
            if (size == 2) {
                daiWeiMap.forEach((s, values) -> {
                    Collections.shuffle(values);
                    Collections.shuffle(values);
                    if (values.size() < 33) {
                        newData.addAll(values);

                    } else {
                        for (int i = 0; i < 32; i++) {
                            newData.add(values.get(i));
                        }
                    }
                });
            }
            if (size == 3) {
                //Set<String> keySet = daiWeiMap.keySet();
                //List<String> list = keySet.stream().collect(Collectors.toList());
                //List<DemoData> demoData = daiWeiMap.get(list.get(list.size() - 1));
                //newData.add(demoData.get(demoData.size()-1));
                //15
                List<List<DemoData>> collect = daiWeiMap.values().stream().collect(Collectors.toList());

                Collections.shuffle(collect);

                List<DemoData> demoData1 = collect.get(collect.size()/2);
//在打乱一遍
                Collections.shuffle(demoData1);

                newData.add(demoData1.get(demoData1.size()/2));

                //daiWeiMap.forEach((s, values) -> {
                //    newData.add(values.get(values.size()-1));
                //});

                daiWeiMap.forEach((s, values) -> {

                    Collections.shuffle(values);
                    Collections.shuffle(values);
                    if (values.size() < 22) {
                        newData.addAll(values);
                    } else {
                        for (int i = 0; i < 21; i++) {
                            newData.add(values.get(i));
                        }
                    }


                });
            }

            if (size == 4) {
                daiWeiMap.forEach((s, values) -> {
                    Collections.shuffle(values);
                    Collections.shuffle(values);
                    if (values.size() < 17) {
                        newData.addAll(values);
                    } else {
                        for (int i = 0; i < 16; i++) {
                            newData.add(values.get(i));
                        }
                    }
                });
            }

            if (size == 5) {

                //Set<String> keySet = daiWeiMap.keySet();
                //List<String> list = keySet.stream().collect(Collectors.toList());
                //List<DemoData> demoData = daiWeiMap.get(list.get(list.size() - 1));
                //newData.add(demoData.get(demoData.size()-1));

                List<List<DemoData>> collect = daiWeiMap.values().stream().collect(Collectors.toList());

                Collections.shuffle(collect);

                List<DemoData> demoData1 = collect.get(collect.size()/2);
                //在打乱一遍
                Collections.shuffle(demoData1);

                newData.add(demoData1.get(demoData1.size()/2));

                daiWeiMap.forEach((s, values) -> {

                    Collections.shuffle(values);
                    Collections.shuffle(values);
                    if (values.size() < 13) {
                        newData.addAll(values);
                    } else {
                        for (int i = 0; i < 12; i++) {
                            newData.add(values.get(i));

                        }

                    }

                });

            }

        });
    }


}
