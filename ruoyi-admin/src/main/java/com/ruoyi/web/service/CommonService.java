package com.ruoyi.web.service;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

public interface CommonService {
    void download(HttpServletResponse response);

    void upload(MultipartFile file);


}
