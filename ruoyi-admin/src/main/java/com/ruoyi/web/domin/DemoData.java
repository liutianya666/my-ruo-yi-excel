package com.ruoyi.web.domin;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode


public class DemoData {

    @ExcelProperty("站址所属市")
    private String city;
    //@ExcelProperty("地市代维维度")
    @ExcelProperty("地市代维")
    private String daiWei;

    @ExcelProperty("站址编码")
    private String string1;
    @ExcelProperty("站址名称")
    private String string2;
    @ExcelProperty("站址运维ID")
    private String string3;

    @ExcelProperty("计划名称")
    private String plan;

    @ExcelProperty("专业")
    private String string4;
    @ExcelProperty("实际巡检人员")
    private String string5;
    @ExcelProperty("实际开始时间")
    private String date1;
    @ExcelProperty("实际结束时间")
    private  String date2;
    @ExcelProperty("签到时间")
    private String string8;

    @ExcelProperty("签到经度")
    private String string9;
    @ExcelProperty("签到维度")
    private String string10;

    @ExcelProperty("距站址距离（米）")
    private String string11;

    @ExcelProperty("状态")
    private String string12;

}
