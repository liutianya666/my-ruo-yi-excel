package com.ruoyi.web.controller.common;
import com.ruoyi.common.annotation.Anonymous;
import lombok.extern.slf4j.Slf4j;
import com.ruoyi.common.utils.file.FileUtils;
import com.ruoyi.web.service.CommonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@Slf4j
@RequestMapping("/myCommon")
@Anonymous
public class MyCommonController {


    @Autowired
    private CommonService commonService;


    /**
     * 文件上传
     * <p>
     * 1. 创建excel对应的实体对象 参照{@link }
     * <p>
     * 2. 由于默认一行行的读取excel，所以需要创建excel一行一行的回调监听器，参照{@link }
     * <p>
     * 3. 直接读即可
     */
    @PostMapping("/upload")
    @ResponseBody

    public String upload(MultipartFile file)  {

        commonService.upload(file);

        return "success";
    }


    /**
     * 下载
     * @param response
     * @throws IOException
     */
    @GetMapping("/download")
    public void download(HttpServletResponse response ) throws IOException {

        commonService.download(response);

    }


    /**
     * 通用下载请求
     *
     * @param filePathName 文件路径
     */
    @GetMapping("common/download")
    public void fileDownload(String filePathName, HttpServletResponse response) {
        try {

             filePathName = "C://Users//Administrator//Desktop//测试.xlsx";
            //String fileName = "测试.xlsx";

            String fileName = filePathName.substring(filePathName.lastIndexOf("/") + 1);

            response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
            FileUtils.setAttachmentResponseHeader(response, fileName);
            FileUtils.writeBytes(filePathName, response.getOutputStream());
        } catch (Exception e) {
            log.error("下载文件失败", e);
        }

    }



}
